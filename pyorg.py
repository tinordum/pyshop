from grab import Grab

class ParserEvents:


    def __init__(self):
        # Создание объекта Grab
        self.g = Grab() 

        # Запрос главной страницы
        self.g.go('https://www.python.org/')

        # Задаем xpath
        self.xpath = '//div[@class="medium-widget event-widget last"]{}'
    
    # Вывод отдельной записи
    def article(self):
        for article in self.g.doc.select(self.xpath.format('//li')):
            print(article.text())
    
    # Проверка наличия класса и имени блока
    def check(self):
        assert 'Upcoming Events' in self.g.doc.select(self.xpath.format('//h2')).text()
        self.article()

if __name__ == "__main__":

    # Инициализация класса
    p = ParserEvents()
    p.check()